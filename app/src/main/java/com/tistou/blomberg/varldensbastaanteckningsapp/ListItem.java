package com.tistou.blomberg.varldensbastaanteckningsapp;


import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/*Vi vill kunna dra i noten så den deletas
Så vi vill ha lyssnare på touches
Så vi väljer att extenda en vy, för vyer lyssnar på touches
linearlayout är en enkel vy, så vi extendar den.
 */
public class ListItem extends RelativeLayout{

    ItemListener mItemListener;
    float initialX;
    float initialTouchX;
    boolean interception = false;
    final static float X_THRESHOLD_INTERCEPT = 40f;
    final static float X_THRESHOLD_DELETE = 200f;

    LinearLayout itemRoot;

    public void setItemRoot(LinearLayout itemRoot) {
        this.itemRoot = itemRoot;
    }

    public interface ItemListener {
        void onListItemClick(ListItem listItem);
        void onListItemSwipeDelete(ListItem listItem);
    }

    public ListItem(Context context) {
        super(context);
        init(context, null, 0);
    }

    public ListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public ListItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {

    }

    public void setItemListener(ItemListener itemListener){
        mItemListener = itemListener;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if(event.getAction() == MotionEvent.ACTION_DOWN){
            initialX = itemRoot.getX();
            initialTouchX = event.getX();
            itemRoot.setBackgroundColor(getResources().getColor(R.color.noteSelected));
        }

        if(event.getAction() == MotionEvent.ACTION_MOVE) {
            float newX = calculateNewX(event);
            if (interception) {
                moveTo(newX);
                if (deleteCheck((int) itemRoot.getX())) {
                    itemRoot.setBackgroundColor(getResources().getColor(R.color.noteDelete));
                }else {
                    itemRoot.setBackgroundColor(getResources().getColor(R.color.noteSelected));
                }

            } else if (checkSwipeIntercept((int) newX)) {
                requestDisallowInterceptTouchEvent(true);
                initialTouchX = event.getX();
                interception = true;
            }
        }

        if(event.getAction() == MotionEvent.ACTION_UP){
            if(interception) {
                if (deleteCheck((int) itemRoot.getX())) {
                    mItemListener.onListItemSwipeDelete(this);
                }
            }else {
                mItemListener.onListItemClick(this);
            }
            interception = false;
            moveTo(initialX);
            itemRoot.setBackgroundColor(getResources().getColor(R.color.noteIdle));
        }

        if(event.getAction() == MotionEvent.ACTION_CANCEL){
            interception = false;
            moveTo(initialX);
            itemRoot.setBackgroundColor(getResources().getColor(R.color.noteIdle));
        }

        //TODO ska jag köra super.onTouchEvent också? Är det för att tillåta barnelement att lyssna?
        //super.onTouchEvent(event);

        return true;
    }

    private void moveTo(float x) {
        RelativeLayout.LayoutParams params = (RelativeLayout .LayoutParams)itemRoot.getLayoutParams();
        params.leftMargin = (int) x;
        itemRoot.setLayoutParams(params);
    }

    private float calculateNewX(MotionEvent event) {
        float deltaTouchX = event.getX() - initialTouchX;
        float newX = getX() + deltaTouchX;
        return newX;
    }

    private boolean checkSwipeIntercept(int newX) {
        if(newX > initialX + X_THRESHOLD_INTERCEPT || newX < initialX - X_THRESHOLD_INTERCEPT){
            return true;
        }else {
            return false;
        }
    }

    private boolean deleteCheck(int newX) {
        if(newX > initialX + X_THRESHOLD_DELETE || newX < initialX - X_THRESHOLD_DELETE){
            return true;
        }else {
            return false;
        }
    }
}
