package com.tistou.blomberg.varldensbastaanteckningsapp;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by umyhblomti on 2015-12-14.
 */
public class SwipeAdapter extends ArrayAdapter<String> implements ListItem.ItemListener {

    ListView listViewRef;
    LayoutInflater mInflater;
    int mResource;
    ArrayList<String> mObjects;
    int mTextViewResourceId;

    ListItemListener mListener;

    public SwipeAdapter(Context context, int resource, int textViewResourceId, ArrayList<String> objects, ListItemListener listener) {
        super(context, resource, textViewResourceId, objects);
        init(context, resource, objects, textViewResourceId, listener);
    }

    public interface ListItemListener{
        void onListItemClickActivity(int position);
        void onListItemSwipeDeleteActivity(int position);
    }

    public void init(Context context, int resource, ArrayList<String> objects, int textViewResourceId, ListItemListener listener){
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        mObjects = objects;
        mTextViewResourceId = textViewResourceId;

        mListener = listener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(listViewRef == null && parent instanceof ListView){
            listViewRef = (ListView) parent;
        }
        ListItem listItem;
        TextView text;

        if (convertView == null) {
            listItem = (ListItem) mInflater.inflate(mResource, parent, false);
        } else {
            listItem = (ListItem) convertView;
        }

        try {
            text = (TextView) listItem.findViewById(mTextViewResourceId);
        } catch (ClassCastException e) {
            Log.e("ArrayAdapter", "You must supply a resource ID for a TextView");
            throw new IllegalStateException(
                    "ArrayAdapter requires the resource ID to be a TextView", e);
        }

        String noteTitle = mObjects.get(position);

        int charsToNewLine = noteTitle.indexOf("\n");
        if(charsToNewLine > -1) {
            //only draw first line + ...
            text.setText(noteTitle.substring(0, charsToNewLine) + " ...");
        } else {
            text.setText(noteTitle);
        }

        listItem.setItemListener(this);

        LinearLayout itemRoot = (LinearLayout) listItem.findViewById(R.id.listitemnote_itemroot);
        listItem.setItemRoot(itemRoot);

        if(noteTitle == ""){
            itemRoot.setBackgroundColor(getContext().getResources().getColor(R.color.noteSelected));
            text.setText("new");
            text.setGravity(Gravity.CENTER);
        }else {
            itemRoot.setBackgroundColor(getContext().getResources().getColor(R.color.noteIdle));
            text.setGravity(Gravity.CENTER_VERTICAL);
        }

        int listWidth = listViewRef.getWidth();
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)itemRoot.getLayoutParams();
        params.width = listWidth;
        itemRoot.setLayoutParams(params);

        return listItem;
    }

    @Override
    public void onListItemClick(ListItem listItem) {
        int position = listViewRef.getPositionForView(listItem);
        mListener.onListItemClickActivity(position);
    }

    @Override
    public void onListItemSwipeDelete(ListItem listItem) {
        int position = listViewRef.getPositionForView(listItem);
        mListener.onListItemSwipeDeleteActivity(position);
    }
}
