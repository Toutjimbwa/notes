package com.tistou.blomberg.varldensbastaanteckningsapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SwipeAdapter.ListItemListener, ScrollAddListView.SwipeListener{

    private static final String TUTORIAL_NOTE_1 = "Drag left or right to remove me!";
    private static final String TUTORIAL_NOTE_2 = "Touch to edit me!\n\nGreat job! If you edit me, changes are saved automatically when you go back or exit the app!";
    private ScrollAddListView listView;
    private SwipeAdapter mAdapter;
    Button undoDelete;

    public static ArrayList<String> notes = new ArrayList<>();
    ArrayList<TrashNote> trash = new ArrayList<>();
    public static final String FILENAME = "NAME";
    public static final String NOTES_KEY = "NOTE";
    public static final String NOTE_INDEX_BUNDLE_KEY = "NOTE_INDEX";
    
    public static SharedPreferences.Editor shPrEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        loadNotesFile();
        setupNoteList();

        undoDelete = (Button)findViewById(R.id.main_undoDelete);
        undoDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                undoNoteToTrash();
            }
        });
    }

    void setupNoteList() {
        mAdapter = new SwipeAdapter(this, R.layout.listitem_note, R.id.listitemnote_tv, notes, this);
        listView = (ScrollAddListView)findViewById(R.id.main_lv_notes);
        listView.setListener(this);
        listView.setDividerHeight(0);
        listView.setAdapter(mAdapter);
        listView.setVerticalFadingEdgeEnabled(true);
    }

    public void noteToTrash(int noteIndex){
        trash.add(0, new TrashNote(notes.get(noteIndex), noteIndex));
        notes.remove(noteIndex);
        mAdapter.notifyDataSetChanged();
        saveNotesToFile();
        undoDelete.setVisibility(View.VISIBLE);
    }

    public void undoNoteToTrash(){
        if(trash.size() > 0){
            notes.add(trash.get(0).myOldPlace, trash.get(0).note);
            mAdapter.notifyDataSetChanged();
            saveNotesToFile();
            trash.remove(0);
        }
        if(trash.size() == 0){
            undoDelete.setVisibility(View.INVISIBLE);
        }
    }

    void saveNotesToFile(){
        Gson gson = new Gson();
        String jsonNotesArray = gson.toJson(notes);
        shPrEditor.putString(NOTES_KEY, jsonNotesArray);
        shPrEditor.commit();
    }

    void loadNotesFile(){
        Gson gson = new Gson();
        SharedPreferences shPr = getSharedPreferences(FILENAME, 0);
        shPrEditor = shPr.edit();
        String jsonNotesArray = shPr.getString(NOTES_KEY, null);
        if(jsonNotesArray == null){
            notes.add(TUTORIAL_NOTE_1);
            notes.add(TUTORIAL_NOTE_2);
            jsonNotesArray = gson.toJson(new ArrayList<String>());
            shPrEditor.putString(NOTES_KEY, jsonNotesArray);
            shPrEditor.commit();
        }else {
            notes = gson.fromJson(jsonNotesArray, (new ArrayList<String>()).getClass());
        }
    }

    void startNoteActivity(int noteIndex){
        Intent noteIntent = new Intent(this, NoteActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(NOTE_INDEX_BUNDLE_KEY, noteIndex);
        noteIntent.putExtras(bundle);
        startActivity(noteIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        trash.clear();
    }

    @Override
    public void onListItemClickActivity(int position) {
        startNoteActivity(position);
    }

    @Override
    public void onListItemSwipeDeleteActivity(int position) {
        noteToTrash(position);
        Toast.makeText(this, "DELETED!", Toast.LENGTH_SHORT).show();
    }

    private void addNote() {
        notes.add(0, "");
        mAdapter.notifyDataSetChanged();
        /*listView.scrollTo(0, 0);
        LinearLayout listItem = (LinearLayout)listView.getChildAt(0);
        listItem.getChildAt(0).setBackgroundColor(getResources().getColor(R.color.noteSelected));
        ((TextView) listItem.findViewById(R.id.listitemnote_tv)).setText("+ New note !");*/
    }

    private void undoAddNote() {
        notes.remove(0);
        mAdapter.notifyDataSetChanged();
        /*listView.scrollTo(0, 0);
        LinearLayout listitem = (LinearLayout)listView.getChildAt(0);
        listitem.getChildAt(0).setBackgroundColor(getResources().getColor(R.color.noteIdle));*/
    }

    @Override
    public void onDownSwipe() {
        addNote();
    }

    @Override
    public void onUnDownSwipe() {
        undoAddNote();
    }

    @Override
    public void onSwipeEnd() {
        Log.i("MINTAG", "onSwipeEnd");
        listView.scrollTo(0, 0);
        notes.set(0, "");
        startNoteActivity(0);
    }
}
