package com.tistou.blomberg.varldensbastaanteckningsapp;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

public class NoteActivity extends AppCompatActivity {

    int theNoteIndex;
    boolean noteInList = false;
    EditText edittNote;
    SharedPreferences.Editor shPrEditor;
    Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        getSupportActionBar().hide();

        edittNote = (EditText)findViewById(R.id.note_editt_thenote);
        theNoteIndex = getIntent().getIntExtra(MainActivity.NOTE_INDEX_BUNDLE_KEY, -1);
        if(theNoteIndex == -1){
            Toast.makeText(this, "ERROR ERROR ERROR!", Toast.LENGTH_LONG).show();
        }
        edittNote.setText(MainActivity.notes.get(theNoteIndex));
        edittNote.setSelection(edittNote.getText().length());
        noteInList = true;
        shPrEditor = MainActivity.shPrEditor;
    }

    void saveNotesToFile(){
        String jsonNotesArray = gson.toJson(MainActivity.notes);
        shPrEditor.putString(MainActivity.NOTES_KEY, jsonNotesArray);
        shPrEditor.commit();
    }

    void saveChanges(){
        String noteString = edittNote.getText().toString();
        if(noteString.equals("")) {
            if (noteInList) {
                noteInList = false;
                boolean showToast = true;
                if(MainActivity.notes.get(theNoteIndex).equals("")){
                    showToast = false;
                }
                MainActivity.notes.remove(theNoteIndex);
                saveNotesToFile();
                if(showToast) {
                    Toast.makeText(this, "Note deleted", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(this, "No note created", Toast.LENGTH_SHORT).show();
                }
            }
        }else if (!noteString.equals(MainActivity.notes.get(theNoteIndex))) {
            if (noteInList) {
                MainActivity.notes.set(theNoteIndex, noteString);
                saveNotesToFile();
                Toast.makeText(this, "Changes saved", Toast.LENGTH_SHORT).show();
            } else {
                noteInList = true;
                MainActivity.notes.add(0, noteString);
                saveNotesToFile();
                Toast.makeText(this, "Changes saved", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveChanges();
    }
}
