package com.tistou.blomberg.varldensbastaanteckningsapp;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ListView;

/**
 * Created by umyhblomti on 2015-12-16.
 */
public class ScrollAddListView extends ListView {

    private static final float NEWNOTE_SWIPE_THRESHOLD = 200;
    float initialY;
    SwipeListener mListener;
    boolean downSwipeHappened = false;
    boolean imInteresstedInTouches = true;
    private boolean getInitialY = false;

    public interface SwipeListener {
        void onDownSwipe();
        void onUnDownSwipe();
        void onSwipeEnd();
    }

    public ScrollAddListView(Context context) {
        super(context);
    }

    public ScrollAddListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScrollAddListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setListener(SwipeListener listener){
        mListener = listener;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        /*
        if begun a dragging
        */
        Log.i("MINTAG","onInteceptTouchEvent()");
        getInitialY = true;
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        int action = ev.getAction();

        if(getInitialY) {
            getInitialY = false;
            Log.i("MINTAG","'ACTION_DOWN'");
            if(this.getChildCount() > 0){
                if (getFirstVisiblePosition() == 0 && this.getChildAt(0).getTop() == 0) {
                    Log.i("MINTAG", "-At Top  initY: " + Float.toString(initialY));
                    imInteresstedInTouches = true;
                    downSwipeHappened = false;
                    initialY = ev.getRawY();
                    super.onTouchEvent(ev);
                    return true;
                } else {
                    imInteresstedInTouches = false;
                }
            } else {
                imInteresstedInTouches = true;
                downSwipeHappened = false;
                initialY = ev.getRawY();
                super.onTouchEvent(ev);
                return true;
            }
        }

        if(imInteresstedInTouches) {
            Log.i("MINTAG","interested");
            if (action == MotionEvent.ACTION_MOVE) {
                Log.i("MINTAG","ACTION_MOVE getY(): " + ev.getRawY());
                if (ev.getRawY() - initialY > NEWNOTE_SWIPE_THRESHOLD) {
                    Log.i("MINTAG","SWIPE THRESHHOLD REACHED");
                    if(!downSwipeHappened) {
                        Log.i("MINTAG","DOWNSWIPE HAPPENED");
                        downSwipeHappened = true;
                        mListener.onDownSwipe();
                    }
                } else {
                    if (downSwipeHappened) {
                        Log.i("MINTAG","UN DOWN SWIPE");
                        downSwipeHappened = false;
                        mListener.onUnDownSwipe();
                    }
                    if(ev.getRawY() <= initialY) {
                        imInteresstedInTouches = false;
                        super.onTouchEvent(ev);
                    }
                }
                return true;
            } else if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL) {
                if(downSwipeHappened) {
                    mListener.onSwipeEnd();
                }
            }
        }
        return super.onTouchEvent(ev);
    }
}
